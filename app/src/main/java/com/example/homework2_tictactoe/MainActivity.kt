package com.example.homework2_tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        whoWon()
        reset()
    }

    private var first_player = true

    private fun move(button: Button) {
        if (first_player){
            button.text = "X"
        }else {
            button.text = "O"
        }
        button.isClickable = false
        first_player = !first_player
    }

    private fun init() {
        b00.setOnClickListener {
            move(b00)
        }
        b01.setOnClickListener {
            move(b01)
        }
        b02.setOnClickListener {
            move(b02)
        }
        b10.setOnClickListener {
            move(b10)
        }
        b11.setOnClickListener {
            move(b11)
        }
        b12.setOnClickListener {
            move(b12)
        }
        b20.setOnClickListener {
            move(b20)
        }
        b21.setOnClickListener {
            move(b21)
        }
        b22.setOnClickListener {
            move(b22)
        }
    }

    private fun whoWon() {
        if (b00.text.toString().isNotEmpty() && b00.text.toString() == b01.text.toString() && b01.text.toString() == b02.text.toString()) {
            showTheWinner(b00.text.toString())
        }else if (b10.text.toString().isNotEmpty() && b10.text.toString() == b11.text.toString() && b11.text.toString() == b12.text.toString()) {
            showTheWinner(b10.text.toString())
        }else if (b20.text.toString().isNotEmpty() && b20.text.toString() == b21.text.toString() && b21.text.toString() == b22.text.toString()) {
            showTheWinner(b20.text.toString())
        }else if (b00.text.toString().isNotEmpty() && b00.text.toString() == b10.text.toString() && b10.text.toString() == b20.text.toString()) {
            showTheWinner(b00.text.toString())
        }else if (b01.text.toString().isNotEmpty() && b01.text.toString() == b11.text.toString() && b11.text.toString() == b21.text.toString()) {
            showTheWinner(b01.text.toString())
        }else if (b02.text.toString().isNotEmpty() && b02.text.toString() == b12.text.toString() && b12.text.toString() == b22.text.toString()) {
            showTheWinner(b02.text.toString())
        }else if (b00.text.toString().isNotEmpty() && b00.text.toString() == b11.text.toString() && b11.text.toString() == b22.text.toString()) {
            showTheWinner(b00.text.toString())
        }else if (b02.text.toString().isNotEmpty() && b02.text.toString() == b11.text.toString() && b11.text.toString() == b20.text.toString()) {
            showTheWinner(b02.text.toString())
        }else if (b00.text.toString().isNotEmpty() && b01.text.toString().isNotEmpty() && b02.text.toString().isNotEmpty() && b10.text.toString().isNotEmpty() && b11.text.toString().isNotEmpty() && b12.text.toString().isNotEmpty() && b20.text.toString().isNotEmpty() && b21.text.toString().isNotEmpty() && b22.text.toString().isNotEmpty()) {
            Toast.makeText(this, "Draw", Toast.LENGTH_SHORT).show()
        }
    }

    private fun showTheWinner(message: String) {
        Toast.makeText(this, "The Winner Is $message", Toast.LENGTH_SHORT).show()
    }

    private fun reset() {
        reset_button.setOnClickListener {
            resetTheButton(b00)
            resetTheButton(b01)
            resetTheButton(b02)
            resetTheButton(b10)
            resetTheButton(b11)
            resetTheButton(b12)
            resetTheButton(b20)
            resetTheButton(b21)
            resetTheButton(b22)
            first_player = true
        }
    }

    private fun resetTheButton(button: Button) {
        button.text = ""
        button.isClickable = true
    }

}